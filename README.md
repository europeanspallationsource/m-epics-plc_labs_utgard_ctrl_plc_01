# PLC

EPICS module to provide communications to/from PLC.

* This is a module specific to a particular Utgard PLC.
* This module contains the database with records that EPICS will read/write to this PLC.
* This module requires a common **s7-plc-comms** module that provide drivers config, etc. This is done using **Makefile** and **requireSnippet** line in **startup.cmd**

If you need to create a new module for EPICS to communicate to a PLC:

1. Create a copy of this module
2. Name accordingly
3. Modify database

## Startup Examples

`iocsh -r plc_lns_lebt_010_vac_plc_1111,2.0.1-catania -c 'requireSnippet(startup.cmd, "PLCNAME=LNS-LEBT-010:VAC-PLC-11111,IPADDR=10.10.1.61,S7DRVPORT=2000,MODBUSDRVPORT=502,INSIZE=2000,RECVTIMEOUT=1000")'`

If ran as proper ioc service:
```
epicsEnvSet(PLCNAME, "LNS-LEBT-010:VAC-PLC-11111")
epicsEnvSet(IPADDR, "10.10.1.61")
epicsEnvSet(S7DRVPORT, "2000")
epicsEnvSet(MODBUSDRVPORT, "502")
epicsEnvSet(INSIZE, "2000")
epicsEnvSet(RECVTIMEOUT, "1000")
require plc_lns_lebt_010_vac_plc_11111, 2.0.1-catania
< ${REQUIRE_plc_lns_lebt_010_vac_plc_11111_PATH}/startup/startup.cmd
```
