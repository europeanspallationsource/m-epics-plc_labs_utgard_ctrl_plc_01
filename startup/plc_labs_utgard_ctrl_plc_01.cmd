
# @field IPADDR
# @type STRING
# PLC IP address

# @field RECVTIMEOUT
# @type INTEGER
# PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)

# @field REQUIRE_plc_labs_utgard_ctrl_plc_01_VERSION
# @runtime YES

# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field REQUIRE_plc_labs_utgard_ctrl_plc_01_PATH
# @runtime YES

# S7 port           : 2000
# Input block size  : 158 bytes
# Output block size : 0 bytes
# Endianness        : BigEndian
s7plcConfigure("plc_labs_utgard_ctrl_plc_01", $(IPADDR), 2000, 158, 0, 1, $(RECVTIMEOUT), 0)

# Modbus port       : 502
drvAsynIPPortConfigure("plc_labs_utgard_ctrl_plc_01", $(IPADDR):502, 0, 0, 1)

# Link type         : TCP/IP (0)
modbusInterposeConfig("plc_labs_utgard_ctrl_plc_01", 0, $(RECVTIMEOUT), 0)

# Slave address     : 0
# Function code     : 16 - Write Multiple Registers
# Addressing        : Absolute (-1)
# Data segment      : 2 words
drvModbusAsynConfigure("plc_labs_utgard_ctrl_plc_01write", "plc_labs_utgard_ctrl_plc_01", 0, 16, -1, 2, 0, 0, "S7-1500")

# Load plc interface database
dbLoadRecords("plc_labs_utgard_ctrl_plc_01.db", "PLCNAME=plc_labs_utgard_ctrl_plc_01, MODVERSION=$(REQUIRE_plc_labs_utgard_ctrl_plc_01_VERSION)")

# Configure autosave
# Number of sequenced backup files to write
save_restoreSet_NumSeqFiles(1)

# Specify directories in which to search for request files
set_requestfile_path("$(REQUIRE_plc_labs_utgard_ctrl_plc_01_PATH)", "misc")

# Specify where the save files should be
set_savefile_path("$(SAVEFILE_DIR)", "")

# Specify what save files should be restored
set_pass0_restoreFile("plc_labs_utgard_ctrl_plc_01.sav")

# Create monitor set
doAfterIocInit("create_monitor_set('plc_labs_utgard_ctrl_plc_01.req', 1, '')")
